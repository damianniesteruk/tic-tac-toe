//***********************************************************************************
// Author: Damian Niesteruk
// E-mail: Xenox93@gmail.com
// Licence: xxx
// File: Texture.cpp
//***********************************************************************************
#include "Texture.h"

Texture::Texture( const char* filename, TypeTexture type, bool &result )
{
    glGenTextures(1,&TextureID); //allocate the memory for texture

    glBindTexture(GL_TEXTURE_2D,TextureID); //Binding the texture


    if( glfwLoadTexture2D( filename, GLFW_BUILD_MIPMAPS_BIT ) )
    {
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
        glTexParameteri( GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST );

        result = true;
    }
    else
    {
        result = false;
    }
}

Texture::~Texture()
{
}
