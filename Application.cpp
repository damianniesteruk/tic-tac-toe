//***********************************************************************************
// Author: Damian Niesteruk
// E-mail: Xenox93@gmail.com
// Licence: xxx
// File: Application.cpp
//***********************************************************************************
#include "Application.h"

Application::Application( SetDevice setDevice, bool &result )
{
    deviceOGL = new DeviceOGL( setDevice, result );
    if( !deviceOGL || !result )
    {
        result = false;
    }
    else
    {
        result = true;
    }
}

Application::~Application()
{
    if( deviceOGL )
    {
        delete deviceOGL;
        deviceOGL = 0;
    }
}
