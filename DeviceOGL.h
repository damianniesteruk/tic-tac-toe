#ifndef DEVICEOGL_H
#define DEVICEOGL_H
//***********************************************************************************
// Author: Damian Niesteruk
// E-mail: Xenox93@gmail.com
// Licence: xxx
// File: DeviceOGL.h
//***********************************************************************************
// INCLUDES
//-----------------------------------------------------------------------------------
#include "Includes.h"

//-----------------------------------------------------------------------------------
struct SetDevice
{
    int Width;
    int Height;
    int FullScreen;
};

//-----------------------------------------------------------------------------------

class DeviceOGL
{
    public:
        DeviceOGL( SetDevice setDevice, bool &result );
        ~DeviceOGL();

        void Begin( float x, float y, float z, float a );
        void End();

        void ChangeSize()
        {
            int width, height;
            glfwGetWindowSize( &width, &height );
            glViewport( 0, 0, width, height );
        }
};

#endif // DEVICEOGL_H
