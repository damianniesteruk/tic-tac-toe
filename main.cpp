//***********************************************************************************
// Author: Damian Niesteruk
// E-mail: Xenox93@gmail.com
// Licence: xxx
// File: main.cpp
//***********************************************************************************
#include "Application.h"

enum StateGame
{
    Play,
    Wait,
    Finish
};

void GLFWCALL handleResize( int width, int height )
{
    glViewport( 0, 0, width, height );

    return;
}

void SaveError( string txt )
{
    fstream save( "error.txt", ios::app );

        save << txt << endl;

    save.close();

    return;
}

//----------------------------------------------------------------------------------------------------------------------------
void CollisionMouseQuads( Input input,  deque<Quad> &quad, StatusQuad &queue, int &j )
{
    int posX = 0;
    int posY = 0;

    if( input.PressMouseButton( GLFW_MOUSE_BUTTON_LEFT ) )
        {
            input.GetMousePos( posX, posY );

            for( int i = 0; i<9; i++ )
            {
                if( posX >= quad[i].GetVertices()[0] && posY >= quad[i].GetVertices()[1] && posX <= quad[i].GetVertices()[2] && posY <= quad[i].GetVertices()[3] )
                {
                    if( quad[i].GetStatus() == empty )
                    {
                        if( queue == circle )
                        {
                            quad[i].SetStatus( circle );
                            queue = cross;
                            j++;
                        }
                        else if( queue == cross )
                        {
                            quad[i].SetStatus( cross );
                            queue = circle;
                            j++;
                        }
                    }
                }
            }
        }

    return;
}

void CollisionMouseMenu( int &i, Input input,  deque<Quad> &quads, Quad replay, Quad end, StateGame &stateGame )
{
    int posX = 0;
    int posY = 0;

    if( input.PressMouseButton( GLFW_MOUSE_BUTTON_LEFT ) && stateGame == Wait )
        {
            input.GetMousePos( posX, posY );

            if( posX >= replay.GetVertices()[0] && posY >= replay.GetVertices()[1] && posX <= replay.GetVertices()[2] && posY <= replay.GetVertices()[3] )
            {
                stateGame = Play;

                for( int i = 0; i<9; i++ )
                {
                    quads[i].SetStatus( empty );
                }

                i = 0;

                glfwSleep( 0.5 );
            }

            if( posX >= end.GetVertices()[0] && posY >= end.GetVertices()[1] && posX <= end.GetVertices()[2] && posY <= end.GetVertices()[3] )
            {
                stateGame = Finish;
                glfwSleep( 0.5 );
            }
        }

    return;
}

StatusQuad WinOrDraw( deque<Quad> quad )
{
    if( quad[0].GetStatus() == cross && quad[1].GetStatus() == cross && quad[2].GetStatus() == cross || quad[0].GetStatus() == cross && quad[3].GetStatus() == cross && quad[6].GetStatus() == cross ||
       quad[0].GetStatus() == cross && quad[4].GetStatus() == cross && quad[8].GetStatus() == cross || quad[1].GetStatus() == cross && quad[4].GetStatus() == cross && quad[7].GetStatus() == cross ||
       quad[2].GetStatus() == cross && quad[5].GetStatus() == cross && quad[8].GetStatus() == cross || quad[3].GetStatus() == cross && quad[4].GetStatus() == cross && quad[5].GetStatus() == cross ||
       quad[6].GetStatus() == cross && quad[7].GetStatus() == cross && quad[8].GetStatus() == cross || quad[2].GetStatus() == cross && quad[4].GetStatus() == cross && quad[6].GetStatus() == cross )
    {
        // Wygrywa krzyzyk
        return cross;
    }
    else if( quad[0].GetStatus() == circle && quad[1].GetStatus() == circle && quad[2].GetStatus() == circle || quad[0].GetStatus() == circle && quad[3].GetStatus() == circle && quad[6].GetStatus() == circle ||
       quad[0].GetStatus() == circle && quad[4].GetStatus() == circle && quad[8].GetStatus() == circle || quad[1].GetStatus() == circle && quad[4].GetStatus() == circle && quad[7].GetStatus() == circle ||
       quad[2].GetStatus() == circle && quad[5].GetStatus() == circle && quad[8].GetStatus() == circle || quad[3].GetStatus() == circle && quad[4].GetStatus() == circle && quad[5].GetStatus() == circle ||
       quad[6].GetStatus() == circle && quad[7].GetStatus() == circle && quad[8].GetStatus() == circle || quad[2].GetStatus() == circle && quad[4].GetStatus() == circle && quad[6].GetStatus() == circle )
    {
        // Wygrywa kolo
        return circle;
    }

    return empty;
}

void DrawQuads( deque<Texture> texture, deque<Quad> quad)
{
    texture[0].ActiveTexture();
    glBegin( GL_TRIANGLES );

        for( int i=0; i<(int)( quad.size() ); i++ )
        {
            if( quad[i].GetStatus() == empty )
            {
                quad[i].Render();
            }
        }

    glEnd();

    //-----------------------------------------
    texture[1].ActiveTexture();
    glBegin( GL_TRIANGLES );

        for( int i=0; i<(int)( quad.size() ); i++ )
        {
            if( quad[i].GetStatus() == circle )
            {
                quad[i].Render();
            }
        }

    glEnd();

    texture[2].ActiveTexture();
    glBegin( GL_TRIANGLES );

        for( int i=0; i<(int)( quad.size() ); i++ )
        {
            if( quad[i].GetStatus() == cross )
            {
                quad[i].Render();
            }
        }

    glEnd();

    return;
}

bool MenuWin( int i, Texture Draw, Texture Circle, Texture Cross, Texture Replay, Texture End, deque<Quad> quad, Quad champion, Quad replay, Quad end )
{
    if( WinOrDraw( quad ) == empty && i == 9 )
    {
        // remis
        Draw.ActiveTexture();

        glBegin( GL_TRIANGLES );

            champion.Render();

        glEnd();

        Replay.ActiveTexture();
        glBegin( GL_TRIANGLES );

            replay.Render();

        glEnd();

        End.ActiveTexture();
        glBegin( GL_TRIANGLES );

            end.Render();

        glEnd();

        return true;
    }
    else if( WinOrDraw( quad ) == cross )
    {
        // krzyzyk
        Cross.ActiveTexture();

        glBegin( GL_TRIANGLES );

            champion.Render();

        glEnd();

        Replay.ActiveTexture();
        glBegin( GL_TRIANGLES );

            replay.Render();

        glEnd();

        End.ActiveTexture();
        glBegin( GL_TRIANGLES );

            end.Render();

        glEnd();

        return true;
    }
    else if( WinOrDraw( quad ) == circle )
    {
        // kolo
        Circle.ActiveTexture();

        glBegin( GL_TRIANGLES );

            champion.Render();

        glEnd();

        Replay.ActiveTexture();
        glBegin( GL_TRIANGLES );

            replay.Render();

        glEnd();

        End.ActiveTexture();
        glBegin( GL_TRIANGLES );

            end.Render();

        glEnd();

        return true;
    }

    return false;
}


void DrawMouse( Input input, Texture Cursor, Quad mouse, int width, int height )
{
    int posX = 0;
    int posY = 0;

    glEnable( GL_BLEND );
    glDisable( GL_DEPTH_TEST );
    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

    Cursor.ActiveTexture();

    glPushMatrix();
    input.UpdateMouse( posX, posY, width, height );

    glBegin( GL_TRIANGLES );

        mouse.Render();

    glEnd();

    glPopMatrix();

    return;
}

//----------------------------------------------------------------------------------------------------------------------------

int main()
{
    int width = 800;
    int height = 600;
    StateGame stateGame = Play;
    Input input;
    StatusQuad queue = circle;

    bool    running = true;

    fstream open( "settings.txt", ios::in );

        open >> width;
        open >> height;

    open.close();

    //---------------------------------------------------------------------------------
    // Initializing
    //---------------------------------------------------------------------------------
    SetDevice setDevice{ width, height, GLFW_FULLSCREEN };
    Application app( setDevice, running );
    if( !running )
    {
        SaveError( "Inicjalizacja urzadzenia OpenGL zakończona niepowodzeniem !!!" );
    }

    deque<Texture> texture;
    texture.push_back( Texture( "bin/button.tga", RGB, running ) );
    texture.push_back( Texture( "bin/kolko.tga", RGB, running ) );
    texture.push_back( Texture( "bin/krzyzyk.tga", RGB, running ) );

    Texture Background( "bin/background.tga", RGB, running );
    Texture Cursor( "bin/cursor.tga", RGBA, running );

    Texture Draw( "bin/draw.tga", RGB, running );
    Texture Circle( "bin/win-circle.tga", RGB, running );
    Texture Cross( "bin/win-cross.tga", RGB, running );

    Texture Replay( "bin/replay.tga", RGB, running );
    Texture End( "bin/close.tga", RGB, running );

    Quad mouse( 0, 0, 32, 32 );
    Quad background( 0, 0, width, height );
    Quad champion( ( width / 4 ), ( height / 4 ), ( width / 2 ) + ( width / 4 ), ( height / 2 ) );

    Quad replay( champion.GetVertices()[0], champion.GetVertices()[3], champion.GetVertices()[0] + ( width / 2 ), champion.GetVertices()[3] + ( height / 8 ) );
    Quad end( replay.GetVertices()[0], replay.GetVertices()[3], replay.GetVertices()[0] + ( width / 2 ), replay.GetVertices()[3] + ( height / 8 ) );

    deque<Quad> quad;
    quad.push_back( Quad( width/8, height/8, ( width/4 ) + ( width/8 ), ( height/4 ) + ( height/8 ) ) );
    quad.push_back( Quad( quad[0].GetVertices()[2], quad[0].GetVertices()[1], ( width / 2 ) + quad[0].GetVertices()[0], quad[0].GetVertices()[3] ) );
    quad.push_back( Quad( quad[1].GetVertices()[2], quad[0].GetVertices()[1], width - quad[0].GetVertices()[0], quad[0].GetVertices()[3] ) );

    quad.push_back( Quad( quad[0].GetVertices()[0], quad[0].GetVertices()[3], quad[0].GetVertices()[2], ( height / 2 ) + ( height/8 ) ) );
    quad.push_back( Quad( quad[1].GetVertices()[0], quad[3].GetVertices()[1], quad[1].GetVertices()[2], quad[3].GetVertices()[3] ) );
    quad.push_back( Quad( quad[2].GetVertices()[0], quad[3].GetVertices()[1], quad[2].GetVertices()[2], quad[3].GetVertices()[3] ) );

    quad.push_back( Quad( quad[3].GetVertices()[0], quad[3].GetVertices()[3], quad[3].GetVertices()[2], height - ( height / 8 ) ) );
    quad.push_back( Quad( quad[4].GetVertices()[0], quad[6].GetVertices()[1], quad[4].GetVertices()[2], quad[6].GetVertices()[3] ) );
    quad.push_back( Quad( quad[5].GetVertices()[0], quad[6].GetVertices()[1], quad[5].GetVertices()[2], quad[6].GetVertices()[3] ) );

    glfwSetWindowSizeCallback(handleResize);

    int i = 0;
    //---------------------------------------------------------------------------------
    // Main Loop
    //---------------------------------------------------------------------------------
    while(running)
    {
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        gluOrtho2D( 0, width, height, 0 );
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        app.GetDeviceOGL()->Begin( 0, 0, 0, 0 );

            glEnable( GL_TEXTURE_2D );

            //-----------------------------------------
            Background.ActiveTexture();

            glBegin( GL_TRIANGLES );

                background.Render();

            glEnd();

            DrawQuads( texture, quad );

            if( MenuWin( i, Draw, Circle, Cross, Replay, End, quad, champion, replay, end ) )
            {
                if( stateGame == Play )
                {
                    stateGame = Wait;
                }

                CollisionMouseMenu( i, input, quad, replay, end, stateGame );
            }
            else
            {
                CollisionMouseQuads( input, quad, queue, i );
            }

            DrawMouse( input, Cursor, mouse, width, height );

            //-----------------------------------------

            glDisable( GL_BLEND );
            glDisable( GL_TEXTURE_2D );

        app.GetDeviceOGL()->End();

        // exit if ESC was pressed or window was closed
        running = !glfwGetKey( GLFW_KEY_ESC ) && glfwGetWindowParam( GLFW_OPENED );

        if( stateGame == Finish )
        {
            running = false;
        }
    }

    getchar();
    cin.ignore();

    texture.clear();
    quad.clear();
    glfwTerminate();

    return 0;
}
