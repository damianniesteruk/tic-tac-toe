//***********************************************************************************
// Author: Damian Niesteruk
// E-mail: Xenox93@gmail.com
// Licence: xxx
// File: Quad.cpp
//***********************************************************************************
#include "Quad.h"

Quad::Quad( float x, float y, float w, float h )
{
    vertices.push_back( float( x ) );
    vertices.push_back( float( y ) );
    vertices.push_back( float( w ) );
    vertices.push_back( float( h ) );

    status = empty;
}

Quad::~Quad()
{
    vertices.clear();
}

void Quad::Render()
{
    glTexCoord2f( 0.0f, 0.0f );
    glVertex3f( vertices[0], vertices[3], 1.0f );
    glTexCoord2f( 1.0f, 1.0f );
    glVertex3f( vertices[2], vertices[1], 1.0f );
    glTexCoord2f( 0.0f, 1.0f );
    glVertex3f( vertices[0], vertices[1], 1.0f );

    glTexCoord2f( 0.0f, 0.0f );
    glVertex3f( vertices[0], vertices[3], 1.0f );
    glTexCoord2f( 1.0f, 0.0f );
    glVertex3f( vertices[2], vertices[3], 1.0f );
    glTexCoord2f( 1.0f, 1.0f );
    glVertex3f( vertices[2], vertices[1], 1.0f );

    return;
}
