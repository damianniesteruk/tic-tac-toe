//***********************************************************************************
// Author: Damian Niesteruk
// E-mail: Xenox93@gmail.com
// Licence: xxx
// File: Includes.h
//***********************************************************************************
#pragma once

//-----------------------------------------------------------------------------------
// INCLUDE
//-----------------------------------------------------------------------------------
#include <deque>
#include <string>
#include <fstream>
#include <iostream>
#include <algorithm>

#include <GL/glfw.h>

using namespace std;
