#ifndef APPLICATION_H
#define APPLICATION_H
//***********************************************************************************
// Author: Damian Niesteruk
// E-mail: Xenox93@gmail.com
// Licence: xxx
// File: Application.h
//***********************************************************************************
// INCLUDES
//-----------------------------------------------------------------------------------
#include "DeviceOGL.h"
#include "Quad.h"
#include "Texture.h"
#include "Shader.h"
#include "Input.h"

//-----------------------------------------------------------------------------------

class Application
{
    public:
        Application( SetDevice setDevice, bool &result );
        ~Application();

        DeviceOGL *GetDeviceOGL()
        {
            return deviceOGL;
        }

    private:
        DeviceOGL *deviceOGL;
};

#endif // APPLICATION_H
