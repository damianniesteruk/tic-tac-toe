#ifndef TEXTURE_H
#define TEXTURE_H
//***********************************************************************************
// Author: Damian Niesteruk
// E-mail: Xenox93@gmail.com
// Licence: xxx
// File: Texture.h
//***********************************************************************************
// INCLUDES
//-----------------------------------------------------------------------------------
#include "Includes.h"

//-----------------------------------------------------------------------------------
// GLOBAL VARIABLES
//-----------------------------------------------------------------------------------
enum TypeTexture
{
	RGB,
	RGBA
};

//-----------------------------------------------------------------------------------

class Texture
{
    public:
        Texture( const char* filename, TypeTexture type, bool &result );
        ~Texture();

        void ActiveTexture()
        {
            glBindTexture( GL_TEXTURE_2D, TextureID );
        }

    private:
        GLuint TextureID;
};

#endif // TEXTURE_H
