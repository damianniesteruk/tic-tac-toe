#ifndef QUAD_H
#define QUAD_H
//***********************************************************************************
// Author: Damian Niesteruk
// E-mail: Xenox93@gmail.com
// Licence: xxx
// File: Quad.h
//***********************************************************************************
// INCLUDES
//-----------------------------------------------------------------------------------
#include "Includes.h"

//-----------------------------------------------------------------------------------
enum StatusQuad
{
    empty,
    cross,
    circle
};

//-----------------------------------------------------------------------------------

class Quad
{
    public:
        Quad( float x, float y, float w, float h );
        ~Quad();

        void Render();

        void SetStatus( StatusQuad st )
        {
            status = st;
        }
        StatusQuad GetStatus()
        {
            return status;
        }

        deque<float> GetVertices()
        {
            return vertices;
        }

    private:
        deque<float> vertices;
        StatusQuad status;
};

#endif // QUAD_H
