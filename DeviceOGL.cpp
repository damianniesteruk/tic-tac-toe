//***********************************************************************************
// Author: Damian Niesteruk
// E-mail: Xenox93@gmail.com
// Licence: xxx
// File: DeviceOGL
//***********************************************************************************
#include "DeviceOGL.h"

DeviceOGL::DeviceOGL( SetDevice setDevice, bool &result )
{
    glfwInit();

    if( !glfwOpenWindow( setDevice.Width, setDevice.Height, 0, 0, 0, 0, 0, 0, setDevice.FullScreen ) )
    {
        glfwTerminate();
        result = false;
    }
    else
    {
        result = true;
    }

    glfwSetWindowTitle("GLFW Application");

    glEnable( GL_CULL_FACE );
}

DeviceOGL::~DeviceOGL()
{
}

void DeviceOGL::Begin( float x, float y, float z, float a )
{
    glClearColor( x, y, z, a );
    glClear( GL_COLOR_BUFFER_BIT );

    return;
}

void DeviceOGL::End()
{
    glfwSwapBuffers();

    return;
}
