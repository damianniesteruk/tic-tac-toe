#ifndef INPUT_H
#define INPUT_H
//***********************************************************************************
// Author: Damian Niesteruk
// E-mail: Xenox93@gmail.com
// Licence: xxx
// File: Input.h
//***********************************************************************************
// INCLUDES
//-----------------------------------------------------------------------------------
#include "Includes.h"

//-----------------------------------------------------------------------------------

class Input
{
    public:
        Input();
        ~Input();

        void GetMousePos( int &posX, int &posY )
        {
            glfwGetMousePos( &posX, &posY );
        }

        void SetMousePos( int posX, int posY )
        {
            glfwSetMousePos( posX, posY );
        }

        void UpdateMouse( int &posX, int &posY, int width, int height )
        {
            GetMousePos( posX, posY );
            if( posX <= 0 || posY <= 0 )
            {
                posX = posY = 0;
            }

            if( posX >= width || posY >= height )
            {
                posX = width;
                posY = height;
            }

            glTranslatef( posX, posY, 0.0f);

            return;
        }

        bool PressMouseButton( int key )
        {
            if( glfwGetMouseButton( key ) == GLFW_PRESS )
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        bool PressKey( int key )
        {
            if( glfwGetKey( key ) == GLFW_PRESS )
            {
                return true;
            }
            else
            {
                return false;
            }
        }
};

#endif // INPUT_H
